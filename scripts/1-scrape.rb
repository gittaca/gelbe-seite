#!/usr/bin/env ruby
# frozen_string_literal: true

require 'nokogiri'
require 'open-uri'

GELBE_LISTE = 'https://www.gelbe-liste.de/lieferengpaesse/archiv'
OUTPUT_FILE = 'Lieferbarkeit.csv'

doc = Nokogiri::HTML4(URI.parse(GELBE_LISTE).open)

# Filtere alle hier relevanten Meldungen
lines = doc.css('a p').select do |p|
  p.text.match(/(Es gibt einen Lieferengpass|Der Lieferengpass .+ ist beendet)/)
end.map(&:text)

# Konvertiere Meldungen in CSV-Zeilen mit ja/nein-Feld für Lieferbarkeit
table = lines.map do |line|
  line
    .sub(';', ',')
    .sub(' - ', ';')
    .sub(/Es gibt einen Lieferengpass ?(für | zu )?/, 'nein;')
    .sub(/Der Lieferengpass (für |zu )?/, 'ja;')
    .sub(/( ist beendet|. Der Her(stel)?).+/, '')
end

File.open(OUTPUT_FILE, 'a') do |file|
  file << table.join("\n")
  file << "\n"
end
