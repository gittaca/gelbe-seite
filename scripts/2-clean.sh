#!/usr/bin/env bash

CSV_SPALTEN='Datum;lieferbar;Präparat'
OUTPUT_FILE=Lieferbarkeit.csv

mv "$OUTPUT_FILE" "$OUTPUT_FILE.ori"

echo -e "$CSV_SPALTEN\n$(cat $OUTPUT_FILE.ori)" |
  sed -E 's;^([0-9]+)\.([0-9]+)\.([0-9]+);\3-\2-\1;g' |
  sort --reverse --unique \
    >"$OUTPUT_FILE"

rm "$OUTPUT_FILE.ori"
