# [Gelbe Liste][gl] der Lieferengpässe von Medikamenten visualisiert

## Motivation

Fast täglich werden in Deutschland Lieferengpässe für Medikamente gemeldet.
Details können über [die "Gelbe Liste" abgerufen][gl] werden.
Jedoch ist für Laien die Gesamtsituation kaum ersichtlich,
oder wie sie sich im Laufe der Zeit verändert.

Von befreundeten Eltern hörte ich letztens,
sie hätten schon einen Wintervorrat an Erkältungsmedikamenten.
Eine gute Idee oder nicht?

## Methodik

Mittels regelmäßiger Auswertung der Meldungen auf [Gelbe-Liste.de][gl]
wird hier dargestellt, wie sich der _tägliche Saldo_ entwickelt:

```plaintext
          Anzahl _wieder_ lieferbarer Präparate
- Anzahl neu _nicht mehr_ lieferbarer Präparate
===============================================
                                täglicher Saldo
```

## Ergebnis

![Lieferbarkeit von Medikamenten im Laufe der Zeit](https://gitlab.com/api/v4/projects/47147786/jobs/artifacts/main/raw/Lieferbarkeit.png?job=scrape)

## Auswertung

Nach einem eher stabilen Herbst/Winter 2022/23
gibt es seit Frühjahr 2023 mehr und mehr Tage,
an denen neue Lieferengpässe gemeldet wurden,
sowie eine tendenziell höhere Anzahl derselben pro Tag.
Demgegenüber stehen weniger Meldungen über Wiederverfügbarkeiten.

Die Gesamtzahl aller verfügbaren Medikamente nimmt daher ab,
und zwar mit zunehmender Geschwindigkeit.

> Vulgo: "Immer mehr schlechte, als gute Nachrichten..."

Die Abhängigkeit von China lässt sich hier gut korrelieren:
Eine Omikron-Welle im Herbst 2022 schien Anlass für das
[schrittweise Aufgeben][sa] der bis dahin meist erfolgreichen
ZeroCoVID-Politik gewesen zu sein.
Es erscheint logisch, dass die darauffolgende, CoVID-bedingte
Disruption von Lieferketten gerade den Pharma-Bereich trifft,
aufgrund seiner hohen Abhängigkeit von chinesischen Zulieferern.

Es sei insb. darauf hingewiesen, dass vor Nov. '22 etwaige
Lieferprobleme sowohl durch CoVID selbst als _auch_ durch ZeroCoVID entstanden.
Nach Ende der chinesischen Maßnahmen können Lieferkettenprobleme
dagegen nur noch der CoVID-Ausbreitung selbst angelastet werden.

## Hinweise

- Meldungen werden vom Hersteller herausgegeben.
  Sie spiegeln also nicht überhohe Nachfrage wieder (oder Ausverkauf),
  sondern Nachschubprobleme.
- Ein einzelnes Medikament kann mehrere Präparate enthalten
  (z.B. unterschiedliche Dosierungen, Darreichungsformen, etc.).
  Eine Meldung bezieht sich nur auf ein Präparat.
  (Bspw.: `MPA 250/500mg` in [der CSV-Datei][cd].)
  Daher bleibt manchmal unklar, ob z.B. auf eine andere,
  noch verfügbare Dosis gewechselt werden kann.
- Die [Gesamtzahl der verfügbaren Präparate][gz] ist natürlich
  deutlich größer als alle hier genannten Zahlen.
  Es wäre also falsch, aus dem obigen Diagramm zu schließen,
  dass bald "keine Medikamente" mehr verfügbar wären.
  Aber der _Trend_ ist deutlich unerfreulich,
  vom Leid auf bestimmte Präparate angewiesener Patienten,
  ganz zu schweigen.

[cd]: https://gitlab.com/gittaca/gelbe-seite/-/blob/main/Lieferbarkeit.csv
[gl]: https://www.gelbe-liste.de/lieferengpaesse
[gz]: https://www.gelbe-liste.de/produkte
[sa]: http://web.archive.org/web/20231014144108/https://nitter.poast.org/brownecfm/status/1608791307358720001
